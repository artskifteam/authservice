package com.picom.security.model;

public enum  Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
